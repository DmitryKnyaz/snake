package sample;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.util.List;
import java.util.function.Consumer;

public class Main extends Application {

    private static final int WIDTH = 600;
    private static final int HEIGHT = WIDTH;

    private static final int ROW = 20;
    private static final int COLUMN = 20;

    private static final int SQUARE_SIZE = WIDTH / ROW;

    private static final int RIGHT = 0;
    private static final int LEFT = 1;
    private static final int UP = 2;
    private static final int DOWN = 3;

    private GraphicsContext gc;

    private int currentDirection;

    Food food ;
    Snake snake;
    Stage primaryStage;
    Timeline timeline;
    Label labelScore;

    @Override
    public void start(Stage primaryStage) {
        food=new Food();
        snake = new Snake();
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Snake");

        labelScore = new Label("Score : " + String.valueOf(snake.getScore()));

        Group root = new Group();

        Canvas canvas = new Canvas(WIDTH, HEIGHT);
        root.getChildren().addAll(canvas, labelScore);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {

                KeyCode code = event.getCode();
                if (code == KeyCode.RIGHT || code == KeyCode.D) {
                    if (currentDirection != LEFT) {
                        currentDirection = RIGHT;
                    }
                } else if (code == KeyCode.LEFT || code == KeyCode.A) {
                    if (currentDirection != RIGHT) {
                        currentDirection = LEFT;
                    }
                } else if (code == KeyCode.UP || code == KeyCode.W) {
                    if (currentDirection != DOWN) {
                        currentDirection = UP;
                    }
                } else if (code == KeyCode.DOWN || code == KeyCode.S) {
                    if (currentDirection != UP) {
                        currentDirection = DOWN;
                    }
                }
            }
        });

        gc = canvas.getGraphicsContext2D();

        food.generateFood(snake.getSnakeBody());

        timeline = new Timeline(new KeyFrame(Duration.millis(110), e -> run()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

    }

    private void run() {
        drawBackground();
        drawFood();
        drawSnake();
        snake.move();

        switch (currentDirection) {
            case RIGHT:
                snake.moveRight();
                break;
            case LEFT:
                snake.moveLeft();
                break;
            case UP:
                snake.moveUp();
                break;
            case DOWN:
                snake.moveDown();
                break;
        }

        snake.eatFood(food, qwe -> labelScore.setText("Score : " + qwe));
        snake.checkRange();
        gameOver();
    }

    public void gameOver() {
        for (int i = 1; i < snake.getSnakeBody().size(); i++) {
            if (snake.getSnakeHead().x == snake.getSnakeBody().get(i).x && snake.getSnakeHead().y ==
                    snake.getSnakeBody().get(i).y) {
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Конец игры");
                Button button = new Button("          GAME OVER" + "\n" + "\n" +
                        "             Score : " + snake.getScore() + "\n" + "\n" + "Нажмите для новой игры");
                button.setOnAction(event -> {
                    start(new Stage());
                    stage.close();
                    timeline.stop();
                });
                Scene scene2 = new Scene(button, 200, 200);
                stage.setScene(scene2);
                stage.show();
                primaryStage.close();
            }
        }
    }

    private void drawBackground() {
        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COLUMN; j++) {
                gc.setFill(Color.LIGHTGREY);
                gc.fillRect(i * SQUARE_SIZE, j * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
            }
        }

    }

    private void drawSnake() {
        gc.setFill(Color.web("4674E9"));
        gc.fillRoundRect(snake.getSnakeHead().getX() * SQUARE_SIZE, snake.getSnakeHead().getY() * SQUARE_SIZE, SQUARE_SIZE - 1, SQUARE_SIZE - 1, 35, 35);

        List<Point> snakeBody = snake.getSnakeBody();
        for (int i = 1; i < snakeBody.size(); i++) {
            gc.fillRoundRect(snakeBody.get(i).getX() * SQUARE_SIZE, snakeBody.get(i).getY() * SQUARE_SIZE, SQUARE_SIZE - 1, SQUARE_SIZE - 1, 20, 20);

        }

    }

    private void drawFood() {
        gc.setFill(Color.RED);
        gc.fillRoundRect(food.getFoodX() * SQUARE_SIZE, food.getFoodY() * SQUARE_SIZE, SQUARE_SIZE - 1, SQUARE_SIZE - 1, 20, 20);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

