package sample;


import javafx.scene.control.Label;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Snake {

    private List<Point> snakeBody = new ArrayList<>();

    private Point snakeHead;
    private int score;

    public int getScore() {
        return score;
    }

    public Point getSnakeHead() {
        return snakeHead;
    }

    public Snake() {
        for (int i = 0; i < 3; i++) {
            snakeBody.add(new Point(5, 20));
        }
        snakeHead = snakeBody.get(0);
    }

    public List<Point> getSnakeBody() {
        return snakeBody;
    }

    public void move() {
        for (int i = snakeBody.size() - 1; i >= 1; i--) {
            snakeBody.get(i).x = snakeBody.get(i - 1).x;
            snakeBody.get(i).y = snakeBody.get(i - 1).y;
        }
    }

    public void eatFood(Food food, Consumer<Integer> callback) {
        if (snakeHead.getX() == food.getFoodX() && snakeHead.getY() == food.getFoodY()) {
            snakeBody.add(new Point(-1, -1));
            score = score + 1;
            System.out.println(2);
            callback.accept(score);
            food.generateFood(snakeBody);
        }
    }

    public void moveRight() {
        snakeHead.x++;
    }

    public void moveLeft() {
        snakeHead.x--;
    }

    public void moveUp() {
        snakeHead.y--;
    }

    public void moveDown() {
        snakeHead.y++;
    }

    public void checkRange() {

        if (snakeHead.x > 19) {
            snakeHead.x = 0;
        }
        if (snakeHead.y > 19) {
            snakeHead.y = 0;
        }
        if (snakeHead.x < 0) {
            snakeHead.x = 20;
        }
        if (snakeHead.y < 0) {
            snakeHead.y = 20;
        }
    }


}
