package sample;


import java.awt.*;
import java.util.List;

public class Food {
    private int foodX;
    private int foodY;

       public void generateFood(List<Point> snakeBody) {

        while (true) {
            foodX = (int) (Math.random() * 20);
            foodY = (int) (Math.random() * 20);
            for (Point snake : snakeBody) {
                if (snake.getX() == foodX && snake.getY() == foodY) {
                    continue;
                }
            }
            break;
        }
    }

    public int getFoodX() {
        return foodX;
    }

    public int getFoodY() {
        return foodY;
    }
}

